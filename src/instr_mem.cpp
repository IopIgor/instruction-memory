#include <systemc.h>
#include "instr_mem.hpp"

using namespace std;

void instr_mem::leggi()
{
    static const unsigned MEM_SIZE = 64;
    static sc_bv<bit_data> line[MEM_SIZE];

    for (unsigned i = 0; i < MEM_SIZE; i++) //memory intialized to 0
      line[i]=0;

    line[1] = "00100000001000010000000000000011"; // addi R1, R1, 3
    line[2] =  "00100000001000100000000000000101"; // addi R2, R1, 5
    line[3] = "10101100001000100000000000000000"; // sw R2, 0(R1)
    line[4] = "10001100001000110000000000000000"; // ld R3, 0(R1)
    line[5] = "00010000010000110000000000000001"; // beq R2, R3, 1
    line[6] = "00000000001000010010100000100000"; // add R5, R1, R1 jumped
    line[7] = "00000000001000100010000000100000"; // add R4, R1, R2
    line[8] = "00010000000000000000000000000001"; // j 1
    line[9] = "00000000001000010010100000100000"; // add R5, R1, R1 jumped
    line[10] = "00000000001000100011000000100000"; // add R6, R1, R2

    while(true)
    {
      wait();
      istruzione->write(line[(indirizzo->read()).to_uint()]);  
    }
}
