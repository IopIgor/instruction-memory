#ifndef INSTR_MEM_HPP
#define INSTR_MEM_HPP

SC_MODULE(instr_mem)
{
	static const unsigned bit_data = 32;

   	sc_in<sc_bv<bit_data> >  indirizzo;
   	sc_out<sc_bv<bit_data> > istruzione;

   	SC_CTOR(instr_mem)
   	{
     	SC_THREAD(leggi);
    		sensitive << indirizzo;
   	}

   private:
   	void leggi();
};

#endif
