#include <systemc.h>
#include <string>
#include "instr_mem.hpp"

using namespace std;

const unsigned bit_data = 32;

SC_MODULE(TestBench)
{
   sc_signal<sc_bv<bit_data> > indirizzo;
   sc_signal<sc_bv<bit_data> > istruzione;

   instr_mem IM;
   
   SC_CTOR(TestBench) : IM("IM")
   {
      SC_THREAD(stimulus_thread);
      IM.indirizzo(this->indirizzo);
      IM.istruzione(this->istruzione);
   }
  
   int check()
   {
      cout << "TEST FINITO" << endl;
      return 0;
   }

   private:

   void stimulus_thread() 
   {
     cout << "START STIMULUS" << endl << endl;
     indirizzo.write(1);
     wait(1,SC_NS);
     cout << "Indirizzo : " << (indirizzo.read()).to_uint() << endl;
     cout << "Istruzione: " << istruzione.read() << endl << endl;   
     indirizzo.write(6);
     wait(1,SC_NS);
     cout << "Indirizzo : " << (indirizzo.read()).to_uint() << endl;
     cout << "Istruzione: " << istruzione.read() << endl << endl;   
     indirizzo.write(14);
     wait(1,SC_NS);
     cout << "Indirizzo : " << (indirizzo.read()).to_uint() << endl;
     cout << "Istruzione: " << istruzione.read() << endl << endl;   
     indirizzo.write(4);
     wait(1,SC_NS);
     cout << "Indirizzo : " << (indirizzo.read()).to_uint() << endl;
     cout << "Istruzione: " << istruzione.read() << endl << endl;  
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START" << endl << endl;

  sc_start();

  return test.check();
} 
